library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity AND2 is
port(x,y:in std_logic;
       z:out std_logic);
end AND2;

architecture Behavioral of AND2 is
begin
z <= x and y;
end Behavioral;