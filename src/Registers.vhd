library IEEE; 
use IEEE.STD_LOGIC_1164.ALL; 
use IEEE.STD_LOGIC_ARITH.ALL; 
use IEEE.STD_LOGIC_UNSIGNED.ALL;  

entity registers is     
Port ( RR1, RR2, WR : in std_logic_vector(4 downto 0);
       WD           : in std_logic_vector(31 downto 0);
       Clk, RegWrite: in std_logic;
       RD1, RD2     : out std_logic_vector(31 downto 0));

end registers; 


architecture Beh of registers is    
type mem_array is array(0 to 63) of std_logic_vector(31 downto 0); 
signal data_mem: mem_array; 
constant zero:integer:=0;
constant t0:integer:=8;
constant s2:integer:=18;
constant s3:integer:=19;

begin 
   mem_process: process(RR1, RR2, WR, WD, RegWrite, Clk)       
  
	variable addrA: integer;   
	variable addrB: integer;   
	variable addrW: integer;    
	variable a:boolean:= true;
	
	
   begin 
	if (a) then
		data_mem(zero) <= (others=>'0');
		data_mem(t0) <= (2=>'1', others=>'0');
		data_mem(s2) <= (3=>'1', 2=>'1', 0=>'1', others=>'0');
		data_mem(s3) <= (2=>'1', others=>'0');
		a := false;
	end if;      
	addrA := conv_integer(RR1(4 downto 0));   
	addrB := conv_integer(RR2(4 downto 0));   
	
	if (Clk='0' or Clk='1') then
	RD1 <= data_mem(addrA);-- after 7 ns;   
	RD2 <= data_mem(addrB);-- after 7 ns;
	end if;
    
	if (RegWrite = '1' and rising_edge(Clk)) then       
		addrW := conv_integer(WR(4 downto 0));  
		if (addrW  /= 0) then
			data_mem(addrW) <= WD;   
		end if;
	end if;  
   end process;  
end Beh; 
