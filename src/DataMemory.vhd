library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity DataMemory is
port(       WriteData:in std_logic_vector(31 downto 0);
              Address:in std_logic_vector(31 downto 0);
 Clk,MemRead,MemWrite:in std_logic;
             ReadData:out std_logic_vector(31 downto 0));
end DataMemory;

architecture Behavioral of DataMemory is
type memory_array is array(natural range <>) of std_logic_vector(7 downto 0);
signal sig_memory: memory_array(0 to 63);
begin
process(Address, Clk, MemRead, MemWrite,sig_memory)
variable memory: memory_array(0 to 63) :=
                               ("UUUUUUUU", "UUUUUUUU", "UUUUUUUU", "UUUUUUUU",
				"11111111", "11111111", "11111111", "11111100", -- -4
				"11111111", "11111111", "11111111", "11111011", -- -5
                                others=>("UUUUUUUU"));
begin
if MemRead /= '1' and MemWrite = '1' and rising_edge(Clk) then
 memory(conv_integer(Address)) := WriteData(31 downto 24);
 memory(conv_integer(Address)+1) := WriteData(23 downto 16);
 memory(conv_integer(Address)+2) := WriteData(15 downto 8);
 memory(conv_integer(Address)+3) := WriteData(7 downto 0);
 ReadData <= "UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU";
elsif MemRead = '1' and MemWrite /= '1' and Address /= "UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU" then
 ReadData <= memory(conv_integer(Address)) & memory(conv_integer(Address)+1) & memory(conv_integer(Address)+2) & memory(conv_integer(Address)+3);
else
 ReadData <= "UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU";
end if;
sig_memory <= memory;
end process;
end Behavioral;
