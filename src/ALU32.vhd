library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- CODE -- OP --
----------------
-- 0000    AND
-- 0001    OR
-- 0010    add
-- 0110    subtract
-- 0111    set on less than
-- 1100    NOR

entity ALU32 is
generic(n:natural:=32);
port(A,B:in std_logic_vector(n-1 downto 0);
	A_invert, B_invert: in std_logic;
	Oper:in std_logic_vector(3 downto 0);
	Result:buffer std_logic_vector(n-1 downto 0);
	Zero, C_out:out std_logic);
 end ALU32;


architecture behavioral of ALU32 is

component ALU1
port(A, B, A_invert, B_invert, C_in : in STD_LOGIC;
	Oper : in STD_LOGIC_VECTOR(3 downto 0);
	Result : out STD_LOGIC; --should be "buffer" instead of "out" ?
	C_out : out std_logic);
end component;

component NOR32 is
    port(
        X: in std_logic_vector(n-1 downto 0);
        Y: out std_logic);
end component;

signal C: std_logic_vector(30 downto 0);
signal temp: std_logic_vector(31 downto 0);

begin

        NOR1 : NOR32 port map(Result, Zero);

	A1: ALU1 port map(A(0), B(0), A_invert, B_invert, '0', Oper, temp(0), C(0));
	G1: for i in 1 to 30 generate
		ALUs: ALU1 port map(A(i), B(i), A_invert, B_invert, C(i-1), Oper, temp(i), C(i));
	end generate;
	A32: ALU1 port map(A(31), B(31), A_invert, B_invert, C(30), Oper, temp(31), C_out);
	
	process(temp, Oper)
	begin
		case Oper is
			when "0111" =>
				if(temp(31) = '1') then
					Result <= "00000000000000000000000000000001";
				else
					Result <= "00000000000000000000000000000000";
				end if;
			when others => Result <= temp;
		end case;
	end process;

end behavioral;
