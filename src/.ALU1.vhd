library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity ALU1 is
port(
    A, B, AInvert, BInvert, CarryIn, Less:in STD_LOGIC;
    Operation:in STD_LOGIC_VECTOR(1 downto 0);
    Result, Set, CarryOut :out STD_LOGIC;
    found_oper: out STD_LOGIC_VECTOR(3 downto 0));
end ALU1;


architecture struc of ALU1 is
  signal atmp:std_logic;
  signal btmp:std_logic;
begin
  
process(A,AInvert)
  begin
  case AInvert is
    when '0' => atmp<=A;
    when others => atmp<=not A;
  end case;
end process;
    
    
process(B,BInvert)
  begin
  case BInvert is
    when '0' => btmp<=B;
    when others => btmp<=not B;
  end case;
end process;
    
    
process(atmp,btmp,CarryIn,Less,Operation)
  variable oper: std_logic_vector(3 downto 0);
begin
  
  
  case Operation is
    when "00" => result <= atmp and btmp;
    when "01" => result <= atmp or btmp;
    when "10" => result <= (atmp xor btmp xor CarryIn); -- Add
                CarryOut <= (atmp and btmp) or (atmp and CarryIn) or (btmp and CarryIn);
                Set<=(atmp and btmp) or (atmp and CarryIn) or (btmp and CarryIn);
    when "11" => result <= Less;
    when others=> result <= '0';
  end case;

end process;

end struc;
