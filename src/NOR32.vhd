library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity NOR32 is
port(x:in std_logic_vector(31 downto 0);
     y:out std_logic);
end NOR32;

architecture Behavioral of NOR32 is
begin
process(x)
variable a:std_logic;
begin
a := x(31);
for i in 30 downto 0 loop
 a := a or x(i);
end loop;
y <= not a;
end process;
end Behavioral;