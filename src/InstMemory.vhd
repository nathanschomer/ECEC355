library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity InstMemory is
port( Address:in std_logic_vector(31 downto 0);
     ReadData:out std_logic_vector(31 downto 0));
end InstMemory;

architecture Behavioral of InstMemory is
begin
process(Address)
type memory_array is array(natural range <>) of std_logic_vector(7 downto 0);
variable memory: memory_array(0 to 1023) :=
				("10001101", "00010101", "00000000", "00000000",  -- lw $s5, 0($t0)
				"10001101", "00010110", "00000000", "00000100",  -- lw $s6, 4($t0)
				"00000010", "10110110", "01111000", "00101010",  -- slt $t7, $s5, $s6
				"00010001", "11100000", "00000000", "00000010",  -- beq $t7, $zero
				"00000010", "01010011", "10001000", "00100010",  -- sub $s1,$s2,$s3 
				"00001000", "00000000", "00000000", "00000111",  -- j exit
				"00000010", "01010011", "10001000", "00100000",  -- L: add $s1,$s2,$s3
				"10101101", "00010001", "00000000", "00001100",  -- exit: sw $s1, 12($t0)
				others=>(others=>'0'));

begin
ReadData <= memory(conv_integer(Address)) & memory(conv_integer(Address)+1) & memory(conv_integer(Address)+2) & memory(conv_integer(Address)+3);
end process;
end Behavioral;
