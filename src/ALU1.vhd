library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- CODE -- OP --
----------------
-- 0000    AND
-- 0001    OR
-- 0010    add
-- 0110    subtract
-- 0111    set on less than
-- 1100    NOR

entity ALU1 is
    port(A, B, A_invert, B_invert, C_in : in STD_LOGIC;
         Oper : in STD_LOGIC_VECTOR(3 downto 0);
         Result : out STD_LOGIC; --should be "buffer" instead of "out" ?
         C_out : out std_logic);
end ALU1;

architecture behavioral of ALU1 is
    signal A_tmp, B_tmp : std_logic;
begin
    
    process(A, A_invert)
    begin
        if A_invert = '1' then
            A_tmp <= not A;
        else
            A_tmp <= A;
        end if;
    end process;

    process(B, B_invert)
    begin
        if B_invert = '1' then
            B_tmp <= not B;
        else
            B_tmp <= B;
        end if;
    end process;
    
    process(A_tmp, B_tmp, C_in, Oper)
    begin
        case Oper is
            when "0000" => result   <= A_tmp AND B_tmp;                 --and
            when "0001" => result   <= A_tmp OR  B_tmp;                 --or
            when "0010" => result   <= A_tmp XOR B_tmp XOR C_in;        --add
							C_out   <= (A_tmp AND B_tmp) OR (A_tmp and C_in) OR (B_tmp AND C_in);
            when "0110" => result   <= A_tmp XOR B_tmp XOR C_in;        --subtract
							C_out   <= ((NOT A_tmp) AND B_tmp) OR ((NOT A_tmp) and C_in) OR (B_tmp AND C_in);
            when "0111" => result   <= A_tmp XOR B_tmp XOR C_in;  --set on less than
							C_out	<= ((NOT A_tmp) AND B_tmp) OR ((NOT A_tmp) and C_in) OR (B_tmp AND C_in);
            when "1100" => result   <= A_tmp NOR B_tmp;                 --NOR
            when others => result   <= '0';
        end case;
    end process;
end behavioral;

