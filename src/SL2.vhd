library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity SL2 is
port(	x:in std_logic_vector(31 downto 0);
		y:out std_logic_vector(31 downto 0)
);
end SL2;

architecture Behavioral of SL2 is
begin
	y <= x(29 downto 0) & "00";
end Behavioral;