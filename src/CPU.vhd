library IEEE;
use ieee.std_logic_1164.all, ieee.numeric_std.all;

entity CPU is
    port(clk : in std_logic; Overflow : out std_logic);
end CPU;

architecture behav of CPU is

    component ALU32 is 
        generic(n:natural:=32);
        port(A,B:in std_logic_vector(n-1 downto 0);
            A_invert, B_invert: in std_logic;
            Oper:in std_logic_vector(3 downto 0);
            Result:buffer std_logic_vector(n-1 downto 0);
            Zero, C_out:out std_logic);
    end component;
    
    component ALUControl is
        port(ALUOp:in std_logic_vector(1 downto 0);
             Funct:in std_logic_vector(5 downto 0);
             Operation:out std_logic_vector(3 downto 0));
    end component;
        
    component AND2 is
        port(x,y:in std_logic;
            z:out std_logic);
    end component;
    
    component control is
        port(Opcode : in std_logic_vector(5 downto 0);
             RegDst,Branch,MemRead,MemtoReg,MemWrite,ALUSrc,RegWrite,Jump:out std_logic;
             ALUOp : out std_logic_vector(1 downto 0));
    end component;
    
    component DataMemory is
        port(WriteData:in std_logic_vector(31 downto 0);
             Address:in std_logic_vector(31 downto 0);
             Clk,MemRead,MemWrite:in std_logic;
             ReadData:out std_logic_vector(31 downto 0));
    end component;
    
    component InstMemory is
        port( Address:in std_logic_vector(31 downto 0);
            ReadData:out std_logic_vector(31 downto 0));
    end component;
    
    component MUX32 is 
    port( x, y : in std_logic_vector(31 downto 0);
         sel : in std_logic;
	 z : out std_logic_vector(31 downto 0));
    end component;
    
    component MUX5 is
        port( x, y : in std_logic_vector(4 downto 0);
            sel : in std_logic;
	    z : out std_logic_vector(4 downto 0));
    end component;

    component PC is 
        port(clk        :in std_logic;
            AddressIn  :in std_logic_vector(31 downto 0);
            AddressOut :out std_logic_vector(31 downto 0));
    end component;
    
    component Registers is 
        Port( RR1, RR2, WR  : in std_logic_vector(4 downto 0);
              WD            : in std_logic_vector(31 downto 0);
              Clk, RegWrite : in std_logic;
              RD1, RD2      : out std_logic_vector(31 downto 0));
    end component;
    
    component SignExtend is
        port(x:in std_logic_vector(15 downto 0);
             y:out std_logic_vector(31 downto 0));
    end component;
    
    component SL2J is
        port(x:in std_logic_vector(25 downto 0);
            y:in std_logic_vector(3 downto 0);
            z:out std_logic_vector(31 downto 0));
    end component;
    
    component SL2 is
        port(x:in std_logic_vector(31 downto 0);
	    y:out std_logic_vector(31 downto 0));
    end component;

    signal ALUOp : std_logic_vector(1 downto 0);
    signal RegDst,Branch,MemRead,MemtoReg,MemWrite,ALUSrc,RegWrite,Jump,Zero : std_logic;
    signal operation : std_logic_vector(3 downto 0);
    signal instruction : std_logic_Vector(31 downto 0);
    
    --we probably wont need all of these temp signals
    signal a,c,d,e,f,g,h,j,k,l,m,n,p,q:std_logic_vector(31 downto 0);
	signal b:std_logic_vector(4 downto 0);
	signal r:std_logic;

begin
    -- Wire Everything Here
	ProgCount: PC port map (clk, p, a);
	l <= std_logic_vector(unsigned(a) + 4); -- add 4
	SigQ: SL2J port map (instruction(25 downto 0), l(31 downto 28), q);
    InstrMem: InstMemory port map(a,instruction);
    ctrl: control port map (instruction(31 downto 26), RegDst, Branch, MemRead, MemtoReg, MemWrite, ALUSrc, RegWrite, Jump, ALUOp);
    SigB: MUX5 port map (instruction(20 downto 16), instruction(15 downto 11), RegDst, b);
	Reg: Registers port map (instruction(25 downto 21), instruction(20 downto 16), b, j, clk, RegWrite, c, d);
    SignExt: SignExtend port map (instruction(15 downto 0), e);
	SigK: SL2 port map (e, k);
	m <= std_logic_vector(unsigned(l) + unsigned(k));
	SigF: MUX32 port map (d,e,ALUSrc,f);
	ALUCont: ALUControl port map (ALUOp,instruction(5 downto 0),operation);
    MainALU: ALU32 port map (c, f, '0', '0', operation, g, Zero);
	r <= Branch and Zero;
	SigN: MUX32 port map (l, m, r, n);
	SigP: MUX32 port map (n, q, Jump, p);
	DataMem: DataMemory port map (d, g, clk, MemRead, MemWrite, h);
	SigJ: MUX32 port map (g, h, MemtoReg, j);
end behav;
