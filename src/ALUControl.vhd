library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity ALUControl is
port(ALUOp:in std_logic_vector(1 downto 0);
	 Funct:in std_logic_vector(5 downto 0);
	 Operation:out std_logic_vector(3 downto 0));
end ALUControl;

architecture Behavior of ALUControl is
begin
	process(ALUOp, Funct)
	begin
		case ALUOp is
			when "00" => Operation <= "0010";
			when "01" => Operation <= "0110";
			when "10" => 
				case Funct is
					when "100000" => Operation <= "0010"; --add
					when "100010" => Operation <= "0110"; --sub
					when "100100" => Operation <= "0000"; --and
					when "100101" => Operation <= "0001"; --or
					when "101010" => Operation <= "0111"; --slt
					when others => Operation <= "UUUU";
				end case;
			when others => Operation <= "UUUU";
		end case;
	end process;
end Behavior;